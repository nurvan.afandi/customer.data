# User API Specs

## Register User

Endpoint : POST /api/users

Request Body :
```json
{
  "username" : "nurvan",
  "password" : "rahasia",
  "name" : "Nurvan Afandi"
}
```

Response Body (Success) :
```json
{
  "data" : "OK"
}
```

Response Body (Failed) :
```json
{
  "errors" : "Username must be not blank"
}
```

## Login User

Endpoint : POST /auth/login

Request Body :
```json
{
  "username" : "nurvan",
  "password" : "rahasia"
}
```

Response Body (Success) :
```json
{
  "data" : {
    "token" : "TOKEN",
    "expiredAt" : 2198715816515 //miliseconds
  }
}
```

Response Body (Failed, 401) :
```json
{
  "errors" : "Username or password wrong"
}
```

## Get User

Endpoint : GET /api/users/current

Request Header :
    
- X-API-TOKEN : TOKEN (Mandatory)

Response Body (Success) :
```json
{
  "data" : {
    "username" : "nurvan",
    "name" : "Nurvan Afandi"
  }
}
```

Response Body (Failed) :
```json
{
  "errors" : "Unauthorized"
}
```

## Update User

Endpoint : PATCH /api/users/current

Request Header :

- X-API-TOKEN : TOKEN (Mandatory)

Request Body :
```json
{
  "name" : "nurvan",
  "password" : "rahasia"
}
```

Response Body (Success) :
```json
{
  "data" : {
    "username" : "nurvan", //put if only want to update name
    "name" : "Nurvan Afandi" //put if only want to update password
  }
}
```

Response Body (Failed) :
```json
{
  "errors" : "Unauthorized"
}
```

## Logout User

Endpoint : DELETE /api/auth/logout

Response Body (Success) :
```json
{
  "data" : "OK"
  }
}
```